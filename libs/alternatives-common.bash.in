# Copyright 2008 Mike Kelly
# Copyright 2009, 2013 David Leverton
# Copyright 2010 Bo Ørsted Andresen
# Distributed under the terms of the GNU General Public License v2

inherit config output path-manipulation

ALTERNATIVESDIR_ROOTLESS="@sysconfdir@/env.d/alternatives"
ALTERNATIVESDIR="${ROOT}${ALTERNATIVESDIR_ROOTLESS}"

get_current_provider() {
    local dieprefix="Could not determine current provider for ${ALTERNATIVE}"
    if [[ -L ${ALTERNATIVESDIR}/${ALTERNATIVE}/_current ]]; then
        local provider=$(readlink "${ALTERNATIVESDIR}/${ALTERNATIVE}/_current" || die "${dieprefix}: readlink ${symlink} failed")
        [[ ${provider} == */* ]] && die "${dieprefix}: malformed target for ${symlink}"

        if [[ -L ${ALTERNATIVESDIR}/${ALTERNATIVE}/${provider} ||
                    ( -e ${ALTERNATIVESDIR}/${ALTERNATIVE}/${provider} && ! -d ${ALTERNATIVESDIR}/${ALTERNATIVE}/${provider} ) ]]; then
            die "${dieprefix}: ${ALTERNATIVESDIR}/${ALTERNATIVE}/${provider} is not a directory"
        fi

        echo "${provider}"

    elif [[ -e ${ALTERNATIVESDIR}/${ALTERNATIVE}/_current ]]; then
        die "${dieprefix}: ${ALTERNATIVESDIR}/${ALTERNATIVE}/_current is not a symlink"
    fi
}

compare_importance() {
    local IFS=.
    local a=( ${1} ) b=( ${2} )
    local -i i=0
    while (( i<${#a[@]} && i<${#b[@]} )); do
        if (( a[i]<b[i] )); then
            return 0
        elif (( a[i]>b[i] )); then
            return 1
        fi
        i+=1
    done
    (( i<${#b[@]} ))
}

sort_providers() {
    local begin=${1:-0}
    local count=${2:-${#providers[@]}}
    [[ ${count} -le 1 ]] && return 0
    sort_providers ${begin} $((count/2))
    sort_providers $((begin+count/2)) $((count-count/2))
    local left=( "${providers[@]:begin:count/2}" )
    local right=( "${providers[@]:begin+count/2:count-count/2}" )
    local -i x i=0 j=0
    for (( x=begin; x<begin+count; ++x )); do
        if (( j>=${#right[@]} )) || { (( i<${#left[@]} )) && compare_importance "${left[i]%%:*}" "${right[j]%%:*}"; }; then
            providers[x]=${left[i++]}
        else
            providers[x]=${right[j++]}
        fi
    done
}

get_providers() {
    local p= importance providers=()
    for p in "${ALTERNATIVESDIR}/${ALTERNATIVE}"/* ; do
        [[ -d ${p} && ! -L ${p} ]] || continue
        p=${p##*/}

        importance=$(< "${ALTERNATIVESDIR}/${ALTERNATIVE}/${p}/_importance")
        importance=${importance:-0}
        [[ "${importance}" =~ ^[0123456789]+(\.[0123456789]+)*$ ]] || die "_importance (${importance}) for ${p} is not a dot-separated list of integers"

        providers+=( "${importance}:${p}" )
    done

    sort_providers
    for (( p=${#providers[@]}-1 ; p>=0 ; --p )); do
        echo "${providers[p]#*:}"
    done
}

_options_parameters() {
    [[ -n ${2} && ${2} != --descriptions ]] && die -q "Unrecognised option ${2}"
    local describe_func=describe_${1#options_}_options descriptions=${2} opt options oldifs=$IFS
    if is_function ${describe_func}; then
        IFS=$'\n'
        options=( $(${describe_func}) )
        IFS=$oldifs
        for opt in "${options[@]}"; do
            [[ ${opt} == --* ]] || continue
            if [[ -n ${descriptions} ]]; then
                echo "${opt/ : /:}"
            else
                echo "${opt%% : *}"
            fi
        done
    fi
}

### set action stub ###

# not available in "eclectic alternatives", but needed by do_update

alternatives_do_set() {
    [[ -z "${ALTERNATIVE}" ]] && die "Need to set ALTERNATIVE in the eclectic module"

    local force provider providers
    if [[ ${1} == --force ]]; then
        force=yes
        shift
    fi

    if [[ ${1} == -+([[:digit:]]) ]]; then
        providers=( $(get_providers) )
        (( ${1#-} <= ${#providers[@]} )) || die -q "The given provider with index (${1#-}) does not exist"
        provider=${providers[${1#-}-1]}
    else
        provider="${1}"
    fi
    [[ -z "${provider}" ]] && die -q "Missing required parameter 'provider'"
    local dieprefix="Could not set provider ${provider} for alternative ${ALTERNATIVE}"

    if [[ ! -d ${ALTERNATIVESDIR}/${ALTERNATIVE}/${provider} ]] ; then
        if is_number ${provider} ; then
            providers=( $(get_providers) )
            [[ -n ${providers[${1#-}-1]} ]] &&
                die -q "The given provider (${provider}) does not exist, did you mean -${provider} (${providers[${1#-}-1]})?"
        fi
        die -q "The given provider (${provider}) does not exist"
    fi

    local symlink newsymlinks=() oldsymlinks=()

    while read -r -d '' symlink; do
        local nicesymlink=${symlink#.}
        nicesymlink=${nicesymlink//+(\/)/\/}
        [[ ${nicesymlink} == /* ]] || die "${dieprefix}: bad symlink ${symlink}?"
        [[ ${nicesymlink} == */ ]] && die "${dieprefix}: bad symlink ${symlink}?"

        newsymlinks+=( "${nicesymlink}" )
    done < <(
        cd "${ALTERNATIVESDIR}/${ALTERNATIVE}/${provider}" || die "${dieprefix}: cd failed"
        find . -type l -print0 | LC_ALL=C sort -r -u -z)
    [[ ${#newsymlinks[@]} -gt 0 ]] || die "${dieprefix}: does not provide any symlinks?"

    if [[ -f ${ALTERNATIVESDIR}/${ALTERNATIVE}/_current_list ]]; then
        while read -r -d '' symlink; do
            local nicesymlink=${symlink//+(\/)/\/}
            [[ ${nicesymlink} == /* ]] || die "${dieprefix}: old provider ${oldcur} provides bad symlink ${symlink}?"
            [[ ${nicesymlink} == */ ]] && die "${dieprefix}: old provider ${oldcur} provides bad symlink ${symlink}?"

            oldsymlinks+=( "${nicesymlink}" )
        done < <(LC_ALL=C sort -r -u -z "${ALTERNATIVESDIR}/${ALTERNATIVE}/_current_list")
        [[ ${#oldsymlinks[@]} -gt 0 ]] || die "${dieprefix}: old provider ${oldcur} does not provide any symlinks?"

    elif [[ -L ${ALTERNATIVESDIR}/${ALTERNATIVE}/_current_list || -e ${ALTERNATIVESDIR}/${ALTERNATIVE}/_current_list ]]; then
        die "${dieprefix}: ${ALTERNATIVESDIR}/${ALTERNATIVE}/_current_list is not a file"
    fi

    local pass errors=
    for pass in check perform; do
        local -i new_i=0 old_i=0
        while [[ -n ${newsymlinks[new_i]} || -n ${oldsymlinks[old_i]} ]]; do

            if ( LC_ALL=C; [[ ${newsymlinks[new_i]} < ${oldsymlinks[old_i]} ]] ); then
                if [[ ${pass} == check ]]; then
                    if [[ -L ${ROOT}${oldsymlinks[old_i]} ]]; then
                        :
                    elif [[ -d ${ROOT}${oldsymlinks[old_i]} ]]; then
                        write_error_msg "Can't remove ${ROOT}${oldsymlinks[old_i]}: is a directory${force:+ which is a fatal error that cannot be ignored by --force}"
                        errors=yes
                    elif [[ -e ${ROOT}${oldsymlinks[old_i]} ]]; then
                        if [[ -n ${force} ]]; then
                            write_warning_msg "Removing ${ROOT}${oldsymlinks[old_i]} due to --force: is not a symlink"
                        else
                            write_error_msg "Refusing to remove ${ROOT}${oldsymlinks[old_i]}: is not a symlink (use --force to override)"
                            errors=yes
                        fi
                    fi

                elif [[ ${pass} == perform ]]; then
                    rm -f "${ROOT}${oldsymlinks[old_i]}" || die "${dieprefix}: rm failed"
                else
                    die "${dieprefix}: unknown \${pass} ${pass}???"
                fi

                old_i+=1

            else
                local target=${ALTERNATIVESDIR_ROOTLESS#/}/${ALTERNATIVE}/_current${newsymlinks[new_i]} dir=${newsymlinks[new_i]%/*}
                while [[ -n ${dir} ]]; do
                    target=../${target}
                    dir=${dir%/*}
                done

                if [[ ${pass} == check ]]; then
                    if [[ -L ${ROOT}${newsymlinks[new_i]} ]]; then
                        :
                    elif [[ -d ${ROOT}${newsymlinks[new_i]} ]]; then
                        write_error_msg "Can't overwrite ${ROOT}${newsymlinks[new_i]}: is a directory${force:+ which is a fatal error that cannot be ignored by --force}"
                        errors=yes
                    elif [[ -e ${ROOT}${newsymlinks[new_i]} ]]; then
                        if [[ -n ${force} ]]; then
                            write_warning_msg "Overwriting ${ROOT}${newsymlinks[new_i]} due to --force: is not a symlink"
                        else
                            write_error_msg "Refusing to overwrite ${ROOT}${newsymlinks[new_i]}: is not a symlink (use --force to override)"
                            errors=yes
                        fi
                    fi

                elif [[ ${pass} == perform ]]; then
                    mkdir -p "${ROOT}${newsymlinks[new_i]%/*}" || die "${dieprefix}: mkdir -p failed"
                    ln -snf "${target#/}" "${ROOT}${newsymlinks[new_i]}" || die "${dieprefix}: ln -snf failed"
                else
                    die "${dieprefix}: unknown \${pass} ${pass}???"
                fi

                [[ ${newsymlinks[new_i]} == ${oldsymlinks[old_i]} ]] && old_i+=1
                new_i+=1
            fi
        done

        [[ -n ${errors} ]] && die "${dieprefix}: see previous errors"
    done

    local oldcur="$(get_current_provider)"
    ln -snf "${provider}" "${ALTERNATIVESDIR}/${ALTERNATIVE}/_current" || die "${dieprefix}: ln -snf failed"

    : >"${ALTERNATIVESDIR}/${ALTERNATIVE}/_current_list" || die "${dieprefix}: emptying/creating _current_list failed"
    for symlink in "${newsymlinks[@]}"; do
        echo -n -e "${symlink}\\0" >>"${ALTERNATIVESDIR}/${ALTERNATIVE}/_current_list" || die "${dieprefix}: appending ${symlink} to _current_list failed"
    done
    return 0
}

### update action ###

# available in both "eclectic alternatives" and individual modules

alternatives_describe_update() {
    echo "Set a default provider if no valid one currently exists"
}

alternatives_describe_update_parameters() {
    echo "[--best] [--ignore] <provider>"
}

alternatives_describe_update_options() {
    echo "--best : update to the best provider even if one is already selected"
    echo "--ignore : update to any valid provider EXCEPT the specified provider"
    echo "<provider> : the name of the provider to use"
}

alternatives_do_update() {
    [[ -z "${ALTERNATIVE}" ]] && die "Need to set ALTERNATIVE in the eclectic module"

    local p cur=$(get_current_provider) providers=( $(get_providers) ) best ignore
    if [[ "--best" == ${1} ]] ; then
        shift
        best=1
    fi
    if [[ "--ignore" == ${1} ]] ; then
        # Try everything except setting the provider to the given
        # one. So, if it isn't the given one, we end up doing
        # nothing. Bug #128
        shift
        ignore=${1}
    fi
    [[ -n ${best} && -n ${1} && -z ${ignore} ]] && die -q "Cannot specify both --best and a provider"

    if [[ -n ${best} ]] ; then
        : # fall through to "switch to first available" loop below
    elif [[ ${cur} == ${1} && -z ${ignore} ]]; then
        # if current provider was just updated, reselect it since it could have changed
        alternatives_do_set "${cur}" && return 0
    elif [[ -n ${cur} && ${cur} != ${ignore} ]] ; then
        # verify existing provider's symlinks
        local p= bad=0
        while read -r -d '' p ; do
            [[ -L "${ROOT}${p}" && -e "${ROOT}${p}" ]] || (( bad++ ))
        done < "${ALTERNATIVESDIR}/${ALTERNATIVE}/_current_list"

        [[ "${bad}" -eq 0 ]] && return 0
        # fix existing provider if possible
        has "${cur}" "${providers[@]}" && alternatives_do_set "${cur}" && return 0
    elif has "${1}" "${providers[@]}" && [[ -z ${ignore} ]] ; then
        # switch to new provider if none was set before or it can't be fixed
        alternatives_do_set "${1}" && return 0
    fi

    # if no valid provider has been selected switch to first available, valid
    # provider, sorted according to importance
    for p in "${providers[@]}"; do
        [[ ${ignore} != ${p} ]] && alternatives_do_set "${p}" && return 0
    done

    # if a provider is set but no providers are available anymore cleanup
    cur=$(get_current_provider)
    if [[ -n ${cur} ]]; then
        alternatives_do_unset "${cur}" && return 2
    fi
    # if no provider is set and none are available that are not ignored, return 2 for cleanup
    [[ -z ${providers[@]} || ${providers[@]} == ${ignore} ]] && return 2

    # we tried everything to select a valid provider, but failed
    return 1
}

alternatives_options_update() {
    _options_parameters ${FUNCNAME#alternatives_} "$@"
    if [[ -n ${ALTERNATIVE} ]]; then
        get_providers
    else
        for alt in ${ALTERNATIVESDIR_ROOTLESS}/_*/*/_importance; do
            echo ${alt} | cut -d/ -f5
        done | sort -u
    fi
}

### unset action stub ###

# not available in "eclectic alternatives", but needed by do_update

alternatives_do_unset() {
    [[ -z "${ALTERNATIVE}" ]] && die "Need to set ALTERNATIVE in the eclectic module"

    local force=
    if [[ ${1} == --force ]]; then
        force=yes
        shift
    fi

    local cur="$(get_current_provider)" p=
    [[ -n "${cur}" ]] || die -q "Nothing to unset"
    local dieprefix="Could not unset provider for ${ALTERNATIVE}"

    local one=false symlink pass errors=
    for pass in check perform; do
        while read -r -d '' symlink; do
            one=true
            if [[ ${pass} == check ]]; then
                if [[ -L ${ROOT}${symlink} ]]; then
                    :
                elif [[ -d ${ROOT}${symlink} ]]; then
                    write_error_msg "Can't remove ${ROOT}${symlink}: is a directory${force:+ which is a fatal error that cannot be ignored by --force}"
                    errors=yes
                elif [[ -e ${ROOT}${symlink} ]]; then
                    if [[ -n ${force} ]]; then
                        write_warning_msg "Removing ${ROOT}${symlink} due to --force: is not a symlink"
                    else
                        write_error_msg "Refusing to remove ${ROOT}${symlink}: is not a symlink (use --force to override)"
                        errors=yes
                    fi
                fi

            elif [[ ${pass} == perform ]]; then
                rm -f "${ROOT}${symlink}" || die "${dieprefix}: rm failed"
            else
                die "${dieprefix}: unknown \${pass} ${pass}???"
            fi
        done <"${ALTERNATIVESDIR}/${ALTERNATIVE}/_current_list"

        [[ -n ${errors} ]] && die "${dieprefix}: see previous errors"
    done

    ${one} || die "${dieprefix}: does not provide any symlinks?"

    rm "${ALTERNATIVESDIR}/${ALTERNATIVE}"/{_current,_current_list} || die "${dieprefix}: rm failed"
}

# vim: set ft=eclectic sw=4 sts=4 ts=4 et tw=80 :
