#!/usr/bin/env bash

# Copyright (c) 2005 Gentoo Foundation.
# Copyright (c) 2008 Ciaran McCreesh
#
# This file is part of the 'eclectic' tools framework.
#
# eclectic is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation; either version 2 of the License, or (at your option) any later
# version.
#
# eclectic is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# eclectic; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA  02111-1307  USA

# check_do function args
# Check that function exists, and call it with args.
check_do() {
    local function="${1}"
    shift
    if is_function "${function}" ; then
        ${function} "$@"
    else
        die "No function ${function}"
    fi
}

trap 'echo "die trap: exiting with error." 1>&2 ; exit 250' SIGUSR1

# die [-q] "Message" PUBLIC
# Display "Message" as an error. If -q is not provided, gives a stacktrace.
die() {
    local item funcname="" sourcefile="" lineno="" n e s="yes"

    # do we have a working write_error_msg?
    if is_function "write_error_msg" ; then
        e="write_error_msg"
    else
        e="echo"
    fi

    # quiet?
    if [[ ${1} == "-q" ]] ; then
        s=""
        shift
    fi

    $e "${@:-(no message)}"

    if [[ -n "${s}" ]] ; then
        echo "Call stack:" 1>&2
        for (( n = 1 ; n < ${#FUNCNAME[@]} ; ++n )) ; do
            funcname=${FUNCNAME[${n}]}
            sourcefile=$(basename ${BASH_SOURCE[${n}]})
            lineno=${BASH_LINENO[$(( n - 1 ))]}
            echo "    * ${funcname} (${sourcefile}:${lineno})" 1>&2
        done
    fi

    echo "die: making eclectic (PID ${ECLECTIC_KILL_TARGET}) exit with error" 1>&2
    kill -s SIGUSR1 "${ECLECTIC_KILL_TARGET}"
    exit 249
}

# do_action action args...
# Load and do 'action' with the specified args
do_action() {
    local action="${1##--}" modfile="" subaction="${2##--}"
    [[ -z ${action} ]] && die "Usage: do_action <action> <args>"
    shift 2

    ECLECTIC_MODULE_NAME="${action}"
    ECLECTIC_COMMAND="${ECLECTIC_PROGRAM_NAME} ${ECLECTIC_MODULE_NAME}"

    [[ ${ECLECTIC_BINARY_NAME##*/} != ${ECLECTIC_PROGRAM_NAME} ]] && \
        ECLECTIC_COMMAND="${ECLECTIC_BINARY_NAME##*/}"

    modfile=$( ec_find_module "${action}" )
    (
        source "$ECLECTIC_DEFAULT_ACTIONS" 2>/dev/null \
            || die "Couldn't source ${ECLECTIC_DEFAULT_ACTIONS}"
        source "${modfile}" 2>/dev/null \
            || die "Couldn't source ${modfile}"
        if [[ -z ${subaction} ]] ; then
            check_do "do_${DEFAULT_ACTION:-usage}" "$@"
        else
            is_function "do_${subaction}" \
                || die -q "Action ${subaction} unknown"
            check_do "do_${subaction}" "$@"
        fi
    )
}

# inherit module PUBLIC
# Sources a given eclectic library file
inherit() {
    local x
    for x in ${@} ; do
        [[ -e "${ECLECTIC_CORE_PATH}/${x}.bash" ]] \
            || die "Couldn't find ${x}.bash"
        source "${ECLECTIC_CORE_PATH}/${x}.bash" \
            || die "Couldn't source ${x}.bash"
    done
}

# make eval not work, because it's evil
eval() {
    die "Don't use eval. Find another way."
}

# GNU sed wrapper (real path to GNU sed determined by configure)
sed() {
    @SED@ "$@"
}

# vim: set ft=eclectic sw=4 sts=4 ts=4 et tw=80 :
